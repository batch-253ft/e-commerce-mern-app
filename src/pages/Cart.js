// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useEffect, useState } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// ========== IMPORT USER-DEFINED MODULES ==========
import CartItemCard from '../components/CartItemCard';



// ========== CODE PROPER ==========
export default function Cart() {

	const [ cartItems, setCartItems ] = useState([]);
	const [ totalAmount, setTotalAmount ] = useState(0);

	useEffect(() => {

		fetch("http://localhost:4000/user/cart", 
			{
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${ localStorage.getItem('token') }`
				}

			})
		.then(res => res.json())
		.then(data => {
			setTotalAmount(data.totalAmount);
		});
	}, [totalAmount]);

	useEffect(() => {

		fetch("http://localhost:4000/user/cart", 
			{
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${ localStorage.getItem('token') }`
				}

	
			})
		.then(res => res.json())
		.then(data => {

			setCartItems(data.items.map(item => {

				return (
					<CartItemCard key={ item._id } itemProp={ item } />
				)
			}));
		});
	}, []);

	return (
		<>
			{ cartItems }
			<h5 className="my-3">Total Amount: { totalAmount }</h5>
			<Button 
			variant="warning"
			as={ Link }
			to='/user/cart/checkout'>
			Checkout
			</Button>
		</>
	)
}