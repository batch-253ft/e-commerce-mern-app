// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useState, useEffect, useContext } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Button, Card, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function AdminCreateProduct() {

	const { user } = useContext(UserContext);

	const [ productName, setProductName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ category, setCategory ] = useState('');
	const [ price, setPrice ] = useState(999999);
	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		if (productName !== '' && description !== '' && category !== '' && price !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [productName, description, category, price]);

	function addProduct(e) {

		e.preventDefault();

		fetch('http://localhost:4000/admin/product/add-item', 
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				category: category,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			if (data.message == 'Item add success') {
				Swal.fire({
					title: "Item Added Successfully!",
					icon: "success",
				});

			} else if (data.message == 'Item already exists') {
				Swal.fire({
					title: "Duplicate Item Found",
					icon: "error",
				});

			} else {
				Swal.fire({
					title: "Error Occurred",
					icon: "error",
					html: "Oops! Something went wrong... <br> Please check your input"
				});
			}
		})
	}

	return (
		<Card className="w-50">
		      <Card.Body>
		        <Card.Title className="text-center">Add Product</Card.Title>
		          	<Form onSubmit={ addProduct }>

		                <Form.Group className="mb-3" controlId="productName">
		                  <Form.Label>Product Name</Form.Label>
		                  <Form.Control 
		                  	type="text" 
		                  	placeholder="Enter Product Name" 
		                  	value={ productName }
		                  	onChange={ e => setProductName(e.target.value) }
		                  	required/>
		                </Form.Group>

		                <Form.Group className="mb-3" controlId="description">
		                  <Form.Label>Description</Form.Label>
		                  <Form.Control 
		                  	as="textarea" 
		                  	rows={3}
		                  	style={{ resize: "none" }}
		                  	placeholder="Enter a short description" 
		                  	value={ description }
		                  	onChange={ e => setDescription(e.target.value) }
		                  	required/>
		                </Form.Group>

		                <Form.Group className="mb-3" controlId="category">
		                  <Form.Label>Category</Form.Label>
		                  <Form.Control 
		                  	type="text" 
		                  	placeholder="Enter Category" 
		                  	value={ category }
		                  	onChange={ e => setCategory(e.target.value) }
		                  	required/>
		                </Form.Group>

		                <Form.Group className="mb-3" controlId="price">
		                  <Form.Label>Price</Form.Label>
		                  <Form.Control 
		                  	type="tel" 
		                  	placeholder="Enter Price" 
		                  	value={ price }
		                  	onChange={ e => setPrice(e.target.value) }
		                  	required/>
		                </Form.Group>

	                      { isActive ? 
	                    		<Button 
		                    		variant="warning" 
		                    		type="submit" 
		                    		id="submitBtn">
	                    		  	ADD PRODUCT
	                    		</Button>
	                    		:
	                    		<Button 
		                    		variant="secondary" 
		                    		type="submit" 
		                    		id="submitBtn" 
		                    		disabled>
	                    		  	ADD PRODUCT
	                    		</Button>
	                		}
		            </Form>
		      </Card.Body>
		    </Card>
	)
}