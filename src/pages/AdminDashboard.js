// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useEffect, useState } from 'react';

// ========== IMPORT USER-DEFINED MODULES ==========
import ProductCard from '../components/ProductCard';



// ========== CODE PROPER ==========
export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {

		fetch("http://localhost:4000/admin/product/all", {
				headers: {
					Authorization: `Bearer ${ localStorage.getItem("token") }`
				}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

				setProducts(data.map(product => {

					return (
						<ProductCard key={ product._id } productProp={ product } />
					)
				}));
			})
	}, []);

	return (
		<>
			{ products }
		</>
	)
}