// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useContext } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Container, Navbar, NavDropdown, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function UserNavbar() {

	const { user } = useContext(UserContext);

	return (
	     <Navbar bg="warning" expand="lg">
	       <Container>
 	       	 <Nav className="me-auto">
              <Nav.Link href="#home">Discover</Nav.Link>
              <Nav.Link href="#link">Categories</Nav.Link>
             </Nav>
	         <Navbar.Brand href="#home">
	           <img
	             alt=""
	             src="/logo.svg"
	             width="30"
	             height="30"
	             className="d-inline-block align-top"
	           />{' '}
	           React Bootstrap Store
	         </Navbar.Brand>
	         <Navbar.Toggle aria-controls="basic-navbar-nav" />
             <Navbar.Collapse id="basic-navbar-nav">
               <Nav className="ms-auto">

               { (user.id == null) ? 
               		<>
		               	<Nav.Link as={ NavLink } to="/user/login" exact="true">Sign In</Nav.Link>
		               	<Nav.Link as={ NavLink } to="/user/register" exact="true">Register</Nav.Link>
	               	</>
	               	:
	                 <NavDropdown title="Account" id="basic-nav-dropdown">
	                   <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
	                   <NavDropdown.Item href="#action/3.2">
	                     Cart
	                   </NavDropdown.Item>
	                   <NavDropdown.Item href="#action/3.3">Settings</NavDropdown.Item>
	                   <NavDropdown.Divider />
	                   <NavDropdown.Item href="#action/3.4">
	                     Sign Out
	                   </NavDropdown.Item>
	                 </NavDropdown>
               }
               </Nav>
             </Navbar.Collapse>
	       </Container>
	     </Navbar>
	)
}