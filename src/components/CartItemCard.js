// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useContext, useEffect, useState } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Button, ButtonToolbar, Dropdown, DropdownButton, Card, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function CartItemCard({ itemProp }) {
	const itemId = itemProp._id;

	const { user } = useContext(UserContext)

	const [ quantity, setQuantity ] = useState(1);
	const [ willEdit, setWillEdit ] = useState(false);

	useEffect(() => {
		setQuantity(itemProp.quantity);
	}, [])

	function confirmEdit() {
		fetch(`http://localhost:4000/user/cart/edit`, 
			{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				items: [
					{
						productId: itemProp.productId,
						quantity: quantity
					}
				]
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			if (data.message == 'Edit Success') {
				window.location.reload();

			} else {
				Swal.fire({
					title: "Error Occurred",
					icon: "error",
					html: "Oops! Something went wrong..."
				});
			}
		})
	}
	

	return (
			<Card>
		     <Card.Body>
		       <Card.Title>
		       	<h5>{ itemProp.name }</h5>
		       </Card.Title>
		       <Card.Subtitle>Subtotal</Card.Subtitle>
		       <Card.Text>{ itemProp.subTotal }</Card.Text>
		       <Card.Subtitle>Quantity</Card.Subtitle>
		       { willEdit ?
		       	 <>
		       	   <ButtonToolbar>
			       	  <Button 
			       	  variant="outline-success"
			       	  onClick={e => setQuantity(quantity - 1)}>
			       	  	-
			       	  </Button>
			       	  <Card.Text>{ quantity }</Card.Text>
			       	  <Button 
			       	  variant="outline-success"
			       	  onClick={e => setQuantity(quantity + 1)}>
			       	  	+
			       	  </Button>
			       </ButtonToolbar>
		       	 </>
		       	:
		       	 <Card.Text>{ quantity }</Card.Text>
		   	   }
		   	   { willEdit ?
		   	   	 <>
			   	   	<Button 
			   	   	variant="success"
			   	   	onClick={ confirmEdit }>
			   	   		SAVE CHANGES
			   	   	</Button>
			   	   	<Button
			   	   	variant="secondary"
			   	   	className="mx-1"
			   	   	onClick={e => setWillEdit(false)}>
			   	   		CANCEL
			   	   	</Button>
			   	 </>
			   	:
			   	 <Button 
			   	 variant="success"
			   	 onClick={e => setWillEdit(true)}>
			   	 EDIT
			   	 </Button>
		   	   }
		     </Card.Body>
		   </Card>
	)
}
