// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useContext } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Accordion, Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function AdminNavbar() {

	const { user } = useContext(UserContext);

	return (
		<Accordion flush>
		     <Accordion.Item eventKey="0">
		       <Accordion.Header>Products</Accordion.Header>
		       <Accordion.Body>
		         
		       </Accordion.Body>
		     </Accordion.Item>

		     <Accordion.Item eventKey="1">
		       <Accordion.Header>Orders</Accordion.Header>
		       <Accordion.Body>
		         
		       </Accordion.Body>
		     </Accordion.Item>

		     <Accordion.Item eventKey="2">
		       <Accordion.Header>Reviews</Accordion.Header>
		       <Accordion.Body>
		         
		       </Accordion.Body>
		     </Accordion.Item>
		   </Accordion>

	)
}